<html>
    <head>
        <meta charset="UTF-8">
        <title>Thành phố MixiCity</title>
    </head>
    <body>
        <?php
            session_start(); // Bắt đầu 1 session để kiểm tra các session trong trang.
            include('connect.php'); // Thêm file connect.php vào index.php để sử dụng các function có trong connect.php
        ?>
        <?php
            if(isset($_SESSION['dangnhapthanhcong'])) { // Kiểm tra session "dangnhapthanhcong" xem có session này tồn tại hay không, nếu có tồn tại thì bạn đã đăng nhập thành công.
                echo 'Xin chào, <b>' . $_SESSION['username'] . '<b>. Email: <i>' . $_SESSION['email'] . '</i><hr>'; // Hiển thị thông tin từ session USERNAME và EMAIL
        ?>
        <form id='formdangxuat' name='formdangxuat' method="POST">
            <button type='submit' name="tienhanhdangxuat">Đăng xuất</button>
        </form>
        <?php
            }
            else // Nếu đăng nhập đéo thành công hoặc chưa đăng nhập sẽ hiển thị form bên dưới (Bao gồm: Đăng ký và Đăng nhập)
            {
        ?>
        <center>
        <form id='formdangky' name='formdangky' method="POST"> <!-- method=GET và POST -->
            Tên tài khoản: <input id='username' name='username' type='text' placeholder="Nhập chữ thôi ba!"><br>
            Mật khẩu: <input id='password' name='password' type='password' placeholder="Nhập chữ thôi ba!"><br>
            Nhập lại Mật khẩu: <input id='password2' name='password2' type='password' placeholder="Nhập lại mật khẩu bên trên nghen!"><br>
            Email: <input id='email' name='email' type='email' placeholder="Nhập chữ thôi ba!"><br>
            <button type='submit' name="tienhanhdangky">Đăng ký</button>
        </form>
        <hr>
        <form id='formdangnhap' name='formdangnhap' method="POST">
            Tên tài khoản: <input id='username' name='username' type='text' placeholder="Nhập chữ thôi ba!"><br>
            Mật khẩu: <input id='password' name='password' type='password' placeholder="Nhập chữ thôi ba!"><br>
            <button type='submit' name="tienhanhdangnhap">Đăng nhập</button>
        </form>
        </center>
        <?php
            }
        ?>
        <?php
            if(isset($_POST['tienhanhdangky'])) { // Kiểm tra nếu như nút Đăng ký đã được bấm
                date_default_timezone_set('Asia/Ho_Chi_Minh'); //Set thời gian của trang web về đúng timezone GMT+7 hoặc của Thành phố Hồ Chí Minh

                $username = $_POST['username']; // Gán biến username = thông tin trong form ở mục username
                $password = $_POST['password']; // Gán biến password = thông tin trong form ở mục password
                $password2 = $_POST['password2']; // Gán biến password2 = thông tin trong form ở mục password2
                $email = $_POST['email']; // Gán biến email = thông tin trong form ở mục email
                $today = date('Y-m-d H:i:s'); // Lấy thời gian hiện tại theo mẫu "Năm - Tháng - Ngày Giờ : Phút : Giây"

                if ($password == $password2) { // Kiểm tra mật khẩu 1 và mật khẩu 2 xem có mật khẩu có giống nhau không
                    $sqlcheckuser = "SELECT * FROM `users` WHERE `username` = '". $username ."'"; // Câu lệnh SQL: Lấy thông tin của bảng USERS từ biến USERNAME
                    $ketqua = $connect->query($sqlcheckuser); // Chạy câu lệnh SQL và lấy kết quả gán vào biến KETQUA
                    if ($ketqua->num_rows > 0) { // Đếm số lượng dòng trùng với thông tin câu lệnh phía trên. Nếu lớn hơn 0 tức là thông tin đã tồn tại.
                        echo 'Tài khoản đã tồn tại.';
                        header("Refresh: 5");
                    }
                    else { // Nếu thông tin chưa tồn tại thì ghi bản ghi người dùng vào trong bảng USERS
                        $sql = "INSERT INTO `users` (`username`, `password`, `email`, `registerdate`) VALUES ('". $username ."', '". md5($password) ."', '". $email ."', '". $today ."')"; // Câu lệnh MYSQL: Ghi thông tin các biến USERNAME, MẬT KHẨU, EMAIL và thời gian hiện tại vào bảng USERS theo thông tin tương ứng trong bảng.
                        if ($connect->query($sql) == TRUE) { // Chạy câu lệnh SQL nếu thành công thì thông báo cho người dùng, đăng ký đã hoàn tất.
                            echo 'Đăng ký hoàn tất.';
                            header("Refresh: 5");
                        } else { // Nếu chạy không thành công thì báo cho người dùng biết, hiện CSDL đang bị lỗi.
                            echo 'Ghi vào CSDL thất bại.';
                            header("Refresh: 5");
                        }
                    }
                } else { // Nếu hai mật khẩu không giống nhau thì thông báo lại cho người dùng, là hai mật khẩu không chính xác.
                    echo 'Hai mật khẩu không chính xác.';
                    header("Refresh: 5");
                }
            }

            if(isset($_POST['tienhanhdangnhap'])) { // Kiểm tra nếu như nút Đăng nhập đã được bấm
                $username = $_POST['username']; // Gán biến username = thông tin trong form ở mục username
                $password = $_POST['password']; // Gán biến password = thông tin trong form ở mục password

                $sqlcheckuser = "SELECT * FROM `users` WHERE `username` = '". $username ."' AND `password` = '". md5($password) ."'"; // Câu lệnh SQL: Lấy thông tin của bảng USERS từ biến USERNAME và PASSWORD (Đã mã hóa MD5)
                $ketqua = $connect->query($sqlcheckuser); // Chạy câu lệnh SQL và lấy kết quả gán vào biến KETQUA

                if ($ketqua->num_rows > 0) { // Đếm số lượng dòng trùng với thông tin câu lệnh phía trên. Nếu lớn hơn 0 tức là thông tin đã tồn tại và đăng nhập thành công.
                    echo 'Đăng nhập thành công.';
                    $user = $ketqua->fetch_array();
                    $_SESSION['dangnhapthanhcong'] = 'OK'; // Gán biến SESSION dangnhapthanhcong để máy hiểu được là đăng nhập đã thành công
                    $_SESSION['username'] = $user["username"]; // Gán biến SESSION username để máy hiểu được đây là thông tin username vừa đăng nhập thành công
                    $_SESSION['email'] = $user["email"]; // Gán biến SESSION email để máy hiểu được đây là thông tin email vừa đăng nhập thành công
                    header("Refresh: 5");
                } else { // Thông tin không trùng khớp với câu lệnh phía trên, báo cho người dùng biết là tên tài khoản hoặc mật khẩu không chính xác
                    echo 'Mật khẩu hoặc tên tài khoản không chính xác.';
                    header("Refresh: 5");
                }
            }

            if(isset($_POST['tienhanhdangxuat'])) { // Kiểm tra nếu như nút Đăng xuất đã được bấm
                session_unset(); //Xóa toàn bộ các biến SESSION đã được gán
                session_destroy(); //Phá hủy toàn bộ các biến SESSION
                header("Refresh: 3");
            }
            /*function DemHoBoMay($solandem) {
                $dem = 0;
                while ($dem < $solandem) {
                    $dem++;
                    echo 'Tôi cần đếm: ' . $solandem . ' - Đếm đến: ' .$dem. ' rồi!<br>';
                }
            }

            $biena = 1;
            $bienb = 2;

            if ($biena + $bienb == 3) {
                DemHoBoMay(20);
            }*/

            /* 10 món đồ -> Tạo 10 ảnh cho 10 món đồ đấy
            echo 'PHP: Xin chào thế giới<br>';
            $biena = 1;
            $bienb = 2;
            $bienc = 10;
            echo 'Biến A: ' . $biena . ' - Biến B: ' . $bienb . '<br>';
            $tongbienavab = $biena + $bienb;
            echo 'Tổng của biến A và B: ' . $tongbienavab . '<br>';

            if($tongbienavab < 3) { //Lớn hơn A > B - Nhỏ hơn A < B - A == A bằng B - Lớn hơn hoặc bằng A >= B - Nhỏ hơn hoặc bằng A <= B
                echo 'Câu trả lời chính xác.<br>';
            } else {
                echo 'Câu trả lời đéo chính xác.<br>';
            }

            for ($i=0; $i < 10; $i++) { 
                echo 'i = ' . $i . '<br>';
            }

            $dem = 0;
            while ($dem < 10) {
                $dem = $dem + 1;
                //$dem++;
                //$dem += 1;
                echo 'Thực thi ' . $dem . '<br>';
            }*/
        ?>
    </body>
</html>